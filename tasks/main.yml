---

- name: Install Memcached
  include_role:
    name: memcached

- name: Install PostgreSQL
  include_role:
    name: postgresql
  vars:
    backup_databases: "{{ db_backup_databases }}"

- name: Install SpamAssassin
  include_role:
    name: spamassassin
  vars:
    service_profile: medium

- name: Ensure database is ready
  meta: flush_handlers

- name: install Mailman 3
  include_role:
    name: mailman3
  vars:
    site_owner_email: "root@{{ domain }}"
    site_admins: "{{ admin_users }}"
    from_email: "listmaster@{{ domain }}"
    webui_name: "{{ display_name }}"
    webui_basedir: "{{ webapp_path }}"
    webui_user: "{{ webapp_user }}"
    domains: "{{ {_webui_vhost: display_name} | combine(extra_vhosts) }}"
    http_vhost: "{{ _webui_vhost }}"
    rest_pw: "{{ mailman_rest_pw }}"
    auth: "{{ social_auth }}"
    # If we use TLS then the canonical host is added as an alias to the ML vhost.
    # This is done to be able to reuse the certificate for SMTP.
    canonical_vhost_nocreate: "{{ use_simple_tls|bool }}"

- name: "Install web vhost '{{ _webui_vhost }}'"
  include_role:
    name: httpd
    tasks_from: vhost
  vars:
    website_domain: "{{ _webui_vhost }}"
    document_root: "{{ webapp_path }}"
    mail_domain: "{{ domain }}"
    content_security_policy: "{{ _content_security_policy }}"
    server_aliases: "{{ extra_vhosts.keys() | list }}"
  when: not use_simple_tls|bool
- name: "Install web vhost '{{ _webui_vhost }}'"
  include_role:
    name: httpd
    tasks_from: vhost
  vars:
    website_domain: "{{ _webui_vhost }}"
    document_root: "{{ webapp_path }}"
    mail_domain: "{{ domain }}"
    content_security_policy: "{{ _content_security_policy }}"
    force_tls: True
    use_tls: True
    use_letsencrypt: True
    # the cert from let's encrypt will have inventory_hostname in the SAN
    # it is useful to reuse the cert for the MTA and MDA
    # also ading extra vhosts
    server_aliases: "{{ ([inventory_hostname] | union(extra_vhosts.keys()|list)) | difference(_webui_vhost) | list }}"
  when: use_simple_tls|bool

- name: Install WSGI server-wide settings
  include_role:
    name: httpd_wsgi
    tasks_from: server
  vars:
    wsgi_allow_app_signals: True

- name: Install WSGI instance for Mailman 3
  include_role:
    name: httpd_wsgi
  vars:
    website_domain: "{{ _webui_vhost }}"
    wsgi_instance: mailman3
    wsgi_script: "{{ webapp_path }}/config/webui.wsgi"
    wsgi_user: "{{ webapp_user }}"
    wsgi_home: "{{ webapp_path }}"

- name: Install Postgrey
  include_role:
    name: postgrey

- name: Install Postfix
  include_role:
    name: postfix
  vars:
    myhostname: "{{ inventory_hostname }}"
    mydomain: "{{ domain }}"
    with_postgrey: true
    with_mailman3: true
    with_spamassassin: true
    smtpd_options:
      content_filter: spamfilter
    aliases: "{{ mail_aliases }}"
  when: not use_simple_tls|bool
- name: Install Postfix
  include_role:
    name: postfix
  vars:
    myhostname: "{{ inventory_hostname }}"
    mydomain: "{{ domain }}"
    with_postgrey: true
    with_mailman3: true
    with_spamassassin: true
    smtpd_options:
      content_filter: spamfilter
    aliases: "{{ mail_aliases }}"
    tls_method: manual
    cert_file: "/etc/letsencrypt/live/{{ _webui_vhost }}/fullchain.pem"
    key_file: "/etc/letsencrypt/live/{{ _webui_vhost }}/privkey.pem"
    ca_file: /etc/pki/tls/cert.pem
  when: use_simple_tls|bool

- name: "Install MDA"
  include_role:
    name: dovecot
  vars:
    auth: yaml-dict
  when: with_dovecot|bool and not use_simple_tls|bool
- name: "Install MDA"
  include_role:
    name: dovecot
  vars:
    auth: yaml-dict
    cert_file: "/etc/letsencrypt/live/{{ _webui_vhost }}/fullchain.pem"
    key_file: "/etc/letsencrypt/live/{{ _webui_vhost }}/privkey.pem"
    ca_file: /etc/pki/tls/cert.pem
  when: with_dovecot|bool and use_simple_tls|bool

# LE hooks order:
#   - 03: deploy cert if needed (copy and change perms for non-root services)
#   - 07: restart/reload/systemctl kill/… to notify the service to take the new cert into account
# other levels for special actions
- name: "Install Let's Encrypt renewal hook"
  template:
    src: "07_mailing_lists_server_cert_renewal_restart_service"
    dest: /etc/letsencrypt/renewal-hooks/deploy/
    owner: root
    group: root
    mode: 0755

